CREATE TABLE IF NOT EXISTS district (
    nr INTEGER UNIQUE NOT NULL,
    name VARCHAR(60) NOT NULL,
    PRIMARY KEY(nr)
);

CREATE TABLE IF NOT EXISTS city (
    iso_code VARCHAR(2) NOT NULL,
    zip VARCHAR(4) NOT NULL,
    zip_additional_nr VARCHAR(2) NOT NULL,
    name VARCHAR(27) NOT NULL,
    search_term VARCHAR(27) NOT NULL,
    canton VARCHAR(2) NOT NULL,
    district_nr INTEGER,
    community_nr VARCHAR(255),
    latitude NUMERIC NOT NULL,
    longitude NUMERIC NOT NULL,
    CONSTRAINT fk_district FOREIGN KEY(district_nr) REFERENCES district(nr),
    CONSTRAINT uniqueness UNIQUE(zip, zip_additional_nr)
);
