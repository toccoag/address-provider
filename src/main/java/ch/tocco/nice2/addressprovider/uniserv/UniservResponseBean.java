package ch.tocco.nice2.addressprovider.uniserv;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UniservResponseBean {

    @JsonProperty("results")
    private List<UniservLocationBean> results;

    public List<UniservLocationBean> getResults() {
        return results;
    }

    public void setResults(List<UniservLocationBean> results) {
        this.results = results;
    }
}
