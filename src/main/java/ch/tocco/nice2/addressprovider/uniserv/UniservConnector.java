package ch.tocco.nice2.addressprovider.uniserv;

import java.net.URI;
import java.time.Duration;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import ch.tocco.nice2.addressprovider.rest.api.Result;

@Component
public class UniservConnector {

    private final RestTemplate restTemplate;

    @Value("${uniserv.api.base_url}")
    private String baseUrl;

    private final LoadingCache<SessionTokenCacheKey, String> sessionTokens = CacheBuilder.newBuilder()
            .expireAfterWrite(Duration.ofMinutes(3))
            .build(new CacheLoader<>() {
                @Override
                public String load(SessionTokenCacheKey key) {
                    return obtainSessionToken(key.customerAuthToken, key.country);
                }
            });

    public UniservConnector(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public List<Result> search(
            String authToken,
            String postcodeStartsWith,
            String cityStartsWith,
            String street,
            String houseNr,
            String country
    ) {
        try {
            String sessionToken = sessionTokens.get(new SessionTokenCacheKey(authToken, country));

            String url = baseUrl + "/autocomplete";

            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(sessionToken);

            AutocompleteRequestBean requestBean = new AutocompleteRequestBean(country, postcodeStartsWith, cityStartsWith, street);

            HttpEntity<AutocompleteRequestBean> requestEntity = new HttpEntity<>(requestBean, headers);

            ResponseEntity<UniservResponseBean> response =
                    restTemplate.exchange(URI.create(url), HttpMethod.POST, requestEntity, UniservResponseBean.class);

            return response.getBody().getResults().stream()
                    .map(locationBean -> {
                        Result result = locationBean.toResult();

                        if (street == null) {
                            // If street is null the search was triggered from a location field which has inputs
                            // for city and postcode only. In this case, we don't want to return suggestions for
                            // streets which Uniserv might provide at this point. We set the street to null everywhere
                            // and then filter out the duplicates using `distinct()` of the stream.
                            result.setStreet(null);
                        } else {
                            result.setHouseNr(houseNr);
                        }

                        return result;
                    })
                    .distinct()
                    .toList();
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    public Result validate(String authToken,
                           Result location,
                           boolean includeCoordinates) {
        try {
            String sessionToken = sessionTokens.get(new SessionTokenCacheKey(authToken, location.getCountry()));

            String url = baseUrl + "/validate";

            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(sessionToken);

            ValidateRequestBean requestBean = new ValidateRequestBean(
                    location.getCountry(),
                    location.getPostcode(),
                    location.getCity(),
                    location.getStreet(),
                    location.getHouseNr(),
                    includeCoordinates
            );

            HttpEntity<ValidateRequestBean> requestEntity = new HttpEntity<>(requestBean, headers);

            ResponseEntity<UniservValidationResponseBean> response = restTemplate.exchange(
                    URI.create(url),
                    HttpMethod.POST,
                    requestEntity,
                    UniservValidationResponseBean.class
            );

            return response.getBody().toResults().get(0);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    @VisibleForTesting
    void clearSessionTokens() {
        sessionTokens.invalidateAll();
    }

    private String obtainSessionToken(String authToken, String country) {
        String url = baseUrl + "/start-session";

        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(authToken);

        SessionTokenRequestBean requestBean = new SessionTokenRequestBean(country);

        HttpEntity<SessionTokenRequestBean> entity = new HttpEntity<>(requestBean, headers);

        ResponseEntity<SessionTokenBean> response = restTemplate.exchange(
                url,
                HttpMethod.POST,
                entity,
                SessionTokenBean.class
        );

        if (!response.getStatusCode().is2xxSuccessful()) {
            throw new RuntimeException("Failed to obtain session token");
        }

        SessionTokenBean bean = response.getBody();
        String sessionToken = bean.getSessionToken();

        if (Strings.isNullOrEmpty(sessionToken)) {
            throw new RuntimeException("Received session token is null or empty");
        }

        return sessionToken;
    }

    private static final class SessionTokenCacheKey {

        private final String customerAuthToken;
        private final String country;

        public SessionTokenCacheKey(String customerAuthToken,
                                    String country) {
            this.customerAuthToken = customerAuthToken;
            this.country = country;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            SessionTokenCacheKey that = (SessionTokenCacheKey) o;
            return customerAuthToken.equals(that.customerAuthToken) && country.equals(that.country);
        }

        @Override
        public int hashCode() {
            return Objects.hash(customerAuthToken, country);
        }
    }
}
