package ch.tocco.nice2.addressprovider.uniserv;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Sets;

public class ValidateRequestBean {

    @JsonProperty("country")
    private String country;
    @JsonProperty("postalCode")
    private String postalCode;
    @JsonProperty("locality")
    private String locality;
    @JsonProperty("street")
    private String street;
    @JsonProperty("houseNumber")
    private String houseNumber;
    @JsonProperty("_options")
    private Options options;

    public ValidateRequestBean(String country,
                               String postalCode,
                               String locality,
                               String street,
                               String houseNumber,
                               boolean includeCoordinates) {
        this.country = country;
        this.postalCode = postalCode;
        this.locality = locality;
        this.street = street;
        this.houseNumber = houseNumber;
        this.options = new Options(includeCoordinates);
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public Options getOptions() {
        return options;
    }

    public void setOptions(Options options) {
        this.options = options;
    }

    public static class Options {

        private String[] include;

        public Options(boolean includeCoordinates) {
            Set<String> includes = Sets.newHashSet("municipality-info", "region-info");
            if (includeCoordinates) {
                includes.add("coordinates-wgs84");
            }
            include = includes.toArray(new String[0]);
        }

        public String[] getInclude() {
            return include;
        }

        public void setInclude(String[] include) {
            this.include = include;
        }
    }
}
