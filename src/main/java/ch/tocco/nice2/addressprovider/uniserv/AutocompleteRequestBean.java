package ch.tocco.nice2.addressprovider.uniserv;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AutocompleteRequestBean {

    @JsonProperty("country")
    private String country;
    @JsonProperty("postalCode")
    private String postalCode;
    @JsonProperty("locality")
    private String locality;
    @JsonProperty("street")
    private String street;

    public AutocompleteRequestBean(String country,
                                   String postalCode,
                                   String locality,
                                   String street) {
        this.country = country;
        this.postalCode = postalCode;
        this.locality = locality;
        this.street = street;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}
