package ch.tocco.nice2.addressprovider.uniserv;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SessionTokenRequestBean {

    @JsonProperty("country")
    private String country;

    public SessionTokenRequestBean(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
