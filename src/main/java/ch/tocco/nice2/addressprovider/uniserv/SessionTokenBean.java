package ch.tocco.nice2.addressprovider.uniserv;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionTokenBean {

    @JsonProperty("_sessionToken")
    private String _sessionToken;

    public String getSessionToken() {
        return _sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this._sessionToken = sessionToken;
    }
}
