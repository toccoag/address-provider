package ch.tocco.nice2.addressprovider.uniserv;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import ch.tocco.nice2.addressprovider.rest.api.Result;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UniservLocationBean {

    @JsonProperty("postalCode")
    private String postalCode;
    @JsonProperty("locality")
    private String locality;
    @JsonProperty("street")
    private String street;
    @JsonProperty("country")
    private String country;

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Result toResult() {
        Result result = new Result(
                street,
                null,
                locality,
                postalCode,
                null,
                null,
                null,
                country,
                null,
                null,
                null
        );

        // Uniserv provides additional data such as the community nr and geo coordinates only
        // in the validation request once a suggestion has been selected, which is why we require
        // the validation call for this suggestion here.
        result.setValidationRequired(true);

        return result;
    }
}
