package ch.tocco.nice2.addressprovider.uniserv;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import ch.tocco.nice2.addressprovider.rest.api.Result;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UniservValidationResponseBean {

    @JsonProperty("resultClass")
    private String resultClass;
    @JsonProperty("results")
    private List<ValidationResult> results;

    public String getResultClass() {
        return resultClass;
    }

    public void setResultClass(String resultClass) {
        this.resultClass = resultClass;
    }

    public List<ValidationResult> getResults() {
        return results;
    }

    public void setResults(List<ValidationResult> results) {
        this.results = results;
    }

    public List<Result> toResults() {
        if (results == null) {
            return Collections.emptyList();
        }
        return results.stream().map(validationResult ->
                new Result(
                        validationResult.getStreet(),
                        validationResult.getHouseNumber(),
                        validationResult.getLocality(),
                        validationResult.getPostalCode(),
                        Optional.ofNullable(validationResult.getRegionInfo())
                                .map(RegionInfo::getIso3166_2Name)
                                .orElse(null),
                        null,
                        Optional.ofNullable(validationResult.getMunicipalityInfo())
                                .map(MunicipalityInfo::getMunicipalityCode)
                                .orElse(null),
                        validationResult.getCountry(),
                        null,
                        Optional.ofNullable(validationResult.getCoordinates())
                                .map(coordinates -> BigDecimal.valueOf(coordinates.getWgs84().getLat()))
                                .orElse(null),
                        Optional.ofNullable(validationResult.getCoordinates())
                                .map(coordinates -> BigDecimal.valueOf(coordinates.getWgs84().getLon()))
                                .orElse(null)
                )).toList();
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ValidationResult {

        @JsonProperty("addressType")
        private String addressType;
        @JsonProperty("country")
        private String country;
        @JsonProperty("postalCode")
        private String postalCode;
        @JsonProperty("locality")
        private String locality;
        @JsonProperty("localityAbbreviation")
        private String localityAbbreviation;
        @JsonProperty("localityAddition")
        private String localityAddition;
        @JsonProperty("subLocality")
        private String subLocality;
        @JsonProperty("subLocalityAbbreviation")
        private String subLocalityAbbreviation;
        @JsonProperty("street")
        private String street;
        @JsonProperty("streetAbbreviation")
        private String streetAbbreviation;
        @JsonProperty("houseNumber")
        private String houseNumber;
        @JsonProperty("regionInfo")
        private RegionInfo regionInfo;
        @JsonProperty("municipalityInfo")
        private MunicipalityInfo municipalityInfo;
        @JsonProperty("coordinates")
        private Coordinates coordinates;

        public String getAddressType() {
            return addressType;
        }

        public void setAddressType(String addressType) {
            this.addressType = addressType;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getPostalCode() {
            return postalCode;
        }

        public void setPostalCode(String postalCode) {
            this.postalCode = postalCode;
        }

        public String getLocality() {
            return locality;
        }

        public void setLocality(String locality) {
            this.locality = locality;
        }

        public String getLocalityAbbreviation() {
            return localityAbbreviation;
        }

        public void setLocalityAbbreviation(String localityAbbreviation) {
            this.localityAbbreviation = localityAbbreviation;
        }

        public String getLocalityAddition() {
            return localityAddition;
        }

        public void setLocalityAddition(String localityAddition) {
            this.localityAddition = localityAddition;
        }

        public String getSubLocality() {
            return subLocality;
        }

        public void setSubLocality(String subLocality) {
            this.subLocality = subLocality;
        }

        public String getSubLocalityAbbreviation() {
            return subLocalityAbbreviation;
        }

        public void setSubLocalityAbbreviation(String subLocalityAbbreviation) {
            this.subLocalityAbbreviation = subLocalityAbbreviation;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getStreetAbbreviation() {
            return streetAbbreviation;
        }

        public void setStreetAbbreviation(String streetAbbreviation) {
            this.streetAbbreviation = streetAbbreviation;
        }

        public String getHouseNumber() {
            return houseNumber;
        }

        public void setHouseNumber(String houseNumber) {
            this.houseNumber = houseNumber;
        }

        public RegionInfo getRegionInfo() {
            return regionInfo;
        }

        public void setRegionInfo(RegionInfo regionInfo) {
            this.regionInfo = regionInfo;
        }

        public MunicipalityInfo getMunicipalityInfo() {
            return municipalityInfo;
        }

        public void setMunicipalityInfo(MunicipalityInfo municipalityInfo) {
            this.municipalityInfo = municipalityInfo;
        }

        public Coordinates getCoordinates() {
            return coordinates;
        }

        public void setCoordinates(Coordinates coordinates) {
            this.coordinates = coordinates;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class RegionInfo {

        @JsonProperty("iso3166_2Name")
        private String iso3166_2Name;
        @JsonProperty("iso3166_2Code")
        private String iso3166_2Code;

        public String getIso3166_2Name() {
            return iso3166_2Name;
        }

        public void setIso3166_2Name(String iso3166_2Name) {
            this.iso3166_2Name = iso3166_2Name;
        }

        public String getIso3166_2Code() {
            return iso3166_2Code;
        }

        public void setIso3166_2Code(String iso3166_2Code) {
            this.iso3166_2Code = iso3166_2Code;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class MunicipalityInfo {

        @JsonProperty("municipalityName")
        private String municipalityName;
        @JsonProperty("municipalityCode")
        private String municipalityCode;

        public String getMunicipalityName() {
            return municipalityName;
        }

        public void setMunicipalityName(String municipalityName) {
            this.municipalityName = municipalityName;
        }

        public String getMunicipalityCode() {
            return municipalityCode;
        }

        public void setMunicipalityCode(String municipalityCode) {
            this.municipalityCode = municipalityCode;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Coordinates {

        @JsonProperty("wgs84")
        private WGS84 wgs84;

        public WGS84 getWgs84() {
            return wgs84;
        }

        public void setWgs84(WGS84 wgs84) {
            this.wgs84 = wgs84;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class WGS84 {

        @JsonProperty("precision")
        private String precision;
        @JsonProperty("lat")
        private double lat;
        @JsonProperty("lon")
        private double lon;

        public String getPrecision() {
            return precision;
        }

        public void setPrecision(String precision) {
            this.precision = precision;
        }

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLon() {
            return lon;
        }

        public void setLon(double lon) {
            this.lon = lon;
        }
    }
}
