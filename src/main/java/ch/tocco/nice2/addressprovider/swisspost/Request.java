package ch.tocco.nice2.addressprovider.swisspost;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Request {

    @JsonProperty("ONRP")
    private int ONRP;
    @JsonProperty("ZipCode")
    private String zipCode;
    @JsonProperty("ZipAddition")
    private String zipAddition = "";
    @JsonProperty("TownName")
    private String townName;
    @JsonProperty("STRID")
    private int STRID;
    @JsonProperty("StreetName")
    private String streetName;
    @JsonProperty("HouseKey")
    private int houseKey;
    @JsonProperty("HouseNo")
    private String houseNo;
    @JsonProperty("HouseNoAddition")
    private String houseNoAddition;

    public Request() {
    }

    public Request(String zipCode,
                   String townName,
                   String streetName,
                   String houseNo,
                   String houseNoAddition) {
        this.zipCode = zipCode;
        this.townName = townName;
        this.streetName = streetName;
        this.houseNo = houseNo;
        this.houseNoAddition = houseNoAddition;
    }

    public int getONRP() {
        return ONRP;
    }

    public void setONRP(int ONRP) {
        this.ONRP = ONRP;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getZipAddition() {
        return zipAddition;
    }

    public void setZipAddition(String zipAddition) {
        this.zipAddition = zipAddition;
    }

    public String getTownName() {
        return townName;
    }

    public void setTownName(String townName) {
        this.townName = townName;
    }

    public int getSTRID() {
        return STRID;
    }

    public void setSTRID(int STRID) {
        this.STRID = STRID;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        streetName = streetName;
    }

    public int getHouseKey() {
        return houseKey;
    }

    public void setHouseKey(int houseKey) {
        this.houseKey = houseKey;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getHouseNoAddition() {
        return houseNoAddition;
    }

    public void setHouseNoAddition(String houseNoAddition) {
        this.houseNoAddition = houseNoAddition;
    }
}
