package ch.tocco.nice2.addressprovider.swisspost;

public class RequestBean {

    private int zipOrderMode;
    private int zipFilterMode;

    private Request request;

    public RequestBean() {
    }

    public RequestBean(String zipCode,
                       String townName,
                       String streetName,
                       String houseNo,
                       String houseNoAddition) {
        request = new Request(zipCode, townName, streetName, houseNo, houseNoAddition);
    }

    public int getZipOrderMode() {
        return zipOrderMode;
    }

    public void setZipOrderMode(int zipOrderMode) {
        this.zipOrderMode = zipOrderMode;
    }

    public int getZipFilterMode() {
        return zipFilterMode;
    }

    public void setZipFilterMode(int zipFilterMode) {
        this.zipFilterMode = zipFilterMode;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
}
