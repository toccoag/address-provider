package ch.tocco.nice2.addressprovider.swisspost;

import java.net.URI;
import java.util.List;

import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import ch.tocco.nice2.addressprovider.rest.api.Result;

@Component
public class SwissPostConnector {

    private final Logger logger = LoggerFactory.getLogger(SwissPostConnector.class);

    private final RestTemplate restTemplate;

    @Value("${swisspost.api.url}")
    private String url;
    @Value("${swisspost.api.username}")
    private String username;
    @Value("${swisspost.api.password}")
    private String password;

    public SwissPostConnector(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public List<Result> search(String postcodeStartsWith, String cityStartsWith, String street, String houseNr) {
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(username, password);

        RequestBean requestBean = new RequestBean(postcodeStartsWith, cityStartsWith, street, houseNr, "");

        HttpEntity<RequestBean> requestEntity = new HttpEntity<>(requestBean, headers);

        List<Result> results = Lists.newArrayList();
        try {
            ResponseEntity<SwissPostResponseBean> response =
                    restTemplate.exchange(URI.create(url), HttpMethod.POST, requestEntity, SwissPostResponseBean.class);
            results = response.getBody().toResults();
        } catch (HttpServerErrorException | ResourceAccessException e) {
            logger.error("Invalid response of Swiss Post API: " + e.getMessage(), e);
        }

        return results;
    }
}
