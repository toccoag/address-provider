package ch.tocco.nice2.addressprovider.swisspost;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Strings;

import ch.tocco.nice2.addressprovider.rest.api.Result;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SwissPostLocationBean {

    @JsonProperty("Canton")
    private String canton;
    @JsonProperty("CountryCode")
    private String countryCode;
    @JsonProperty("HouseKey")
    private String houseKey;
    @JsonProperty("HouseNo")
    private String houseNo;
    @JsonProperty("HouseNoAddition")
    private String houseNoAddition;
    @JsonProperty("ONRP")
    private String ONRP;
    @JsonProperty("StreetName")
    private String streetName;
    @JsonProperty("TownName")
    private String townName;
    @JsonProperty("ZipAddition")
    private String zipAddition;
    @JsonProperty("ZipCode")
    private String zipCode;

    public String getCanton() {
        return canton;
    }

    public void setCanton(String canton) {
        this.canton = canton;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getHouseKey() {
        return houseKey;
    }

    public void setHouseKey(String houseKey) {
        this.houseKey = houseKey;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getHouseNoAddition() {
        return houseNoAddition;
    }

    public void setHouseNoAddition(String houseNoAddition) {
        this.houseNoAddition = houseNoAddition;
    }

    public String getONRP() {
        return ONRP;
    }

    public void setONRP(String ONRP) {
        this.ONRP = ONRP;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getTownName() {
        return townName;
    }

    public void setTownName(String townName) {
        this.townName = townName;
    }

    public String getZipAddition() {
        return zipAddition;
    }

    public void setZipAddition(String zipAddition) {
        this.zipAddition = zipAddition;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Result toResult() {
        String houseNr = houseNo;
        if (!Strings.isNullOrEmpty(houseNoAddition)) {
            houseNr += houseNoAddition;
        }
        return new Result(streetName, houseNr, townName, zipCode, null, null, null, countryCode, zipAddition, null, null);
    }
}
