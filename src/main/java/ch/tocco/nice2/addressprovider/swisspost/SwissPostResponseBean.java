package ch.tocco.nice2.addressprovider.swisspost;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import ch.tocco.nice2.addressprovider.rest.api.Result;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SwissPostResponseBean {

    @JsonProperty("QueryAutoComplete4Result")
    public QueryAutoComplete4Result queryAutoComplete4Result;

    public QueryAutoComplete4Result getQueryAutoComplete4Result() {
        return queryAutoComplete4Result;
    }

    public void setQueryAutoComplete4Result(QueryAutoComplete4Result queryAutoComplete4Result) {
        this.queryAutoComplete4Result = queryAutoComplete4Result;
    }

    public List<Result> toResults() {
        return queryAutoComplete4Result.getAutoCompleteResult().stream()
                .map(SwissPostLocationBean::toResult)
                .collect(Collectors.toList());
    }

    public static class QueryAutoComplete4Result {

        @JsonProperty("AutoCompleteResult")
        private List<SwissPostLocationBean> autoCompleteResult;

        public List<SwissPostLocationBean> getAutoCompleteResult() {
            return autoCompleteResult;
        }

        public void setAutoCompleteResult(List<SwissPostLocationBean> autoCompleteResult) {
            this.autoCompleteResult = autoCompleteResult;
        }
    }
}
