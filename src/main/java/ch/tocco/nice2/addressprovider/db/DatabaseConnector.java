package ch.tocco.nice2.addressprovider.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import ch.tocco.nice2.addressprovider.rest.api.Result;

@Component
public class DatabaseConnector {

    private final NamedParameterJdbcTemplate namedJdbcTemplate;

    @Value("${address-provider.limit}")
    private int limit;

    private List<String> supportedCountries = null;

    public DatabaseConnector(NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.namedJdbcTemplate = namedJdbcTemplate;
    }

    public List<Result> search(String postcodeStartsWith, String cityStartsWith, List<String> countries) {
        SqlParameterSource parameters = new MapSqlParameterSource(
                Map.of("countries", countries,
                        "limit", limit,
                        "postcodeStartsWith", postcodeStartsWith + "%",
                        "cityStartsWith", cityStartsWith.toUpperCase() + "%"));

        return namedJdbcTemplate.query(
                "SELECT c.iso_code, c.zip, c.zip_additional_nr, c.name, c.canton, d.name AS district_name, c.community_nr, c.latitude, c.longitude " +
                        "FROM city AS c " +
                        "LEFT JOIN district AS d ON c.district_nr = d.nr " +
                        "WHERE c.iso_code IN (:countries) AND c.zip LIKE :postcodeStartsWith AND c.search_term LIKE :cityStartsWith " +
                        "ORDER BY c.name, c.zip " +
                        "LIMIT :limit",
                parameters,
                new ResultMapper());
    }

    public List<Result> getAll() {
        return namedJdbcTemplate.query(
                "SELECT c.iso_code, c.zip, c.zip_additional_nr, c.name, c.canton, d.name AS district_name, c.community_nr, c.latitude, c.longitude " +
                        "FROM city AS c " +
                        "LEFT JOIN district AS d ON c.district_nr = d.nr " +
                        "ORDER BY c.zip, c.name",
                new MapSqlParameterSource(),
                new ResultMapper());
    }

    public List<String> getSupportedCountries() {
        if (supportedCountries == null) {
            supportedCountries = namedJdbcTemplate.queryForList("SELECT iso_code FROM city GROUP BY iso_code", Map.of(), String.class);
        }
        return supportedCountries;
    }

    public void supplementData(List<Result> results) {
        results.forEach(result -> {
            List<Result> dbResults = search(result.getPostcode(), result.getCity(), List.of(result.getCountry()));

            if (!dbResults.isEmpty()) {
                Result firstResult = dbResults.get(0);

                result.setState(firstResult.getState());
                result.setDistrict(firstResult.getDistrict());
                result.setCommunityNr(firstResult.getCommunityNr());
            }
        });
    }

    private static final class ResultMapper implements RowMapper<Result> {
        public Result mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Result(
                    null,
                    null,
                    rs.getString("name"),
                    rs.getString("zip"),
                    rs.getString("canton"),
                    rs.getString("district_name"),
                    rs.getString("community_nr"),
                    rs.getString("iso_code"),
                    rs.getString("zip_additional_nr"),
                    rs.getBigDecimal("latitude"),
                    rs.getBigDecimal("longitude")
            );
        }
    }
}
