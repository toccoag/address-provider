package ch.tocco.nice2.addressprovider.rest.api;

import java.util.List;

public class ResultBean {

    private List<Result> results;

    public ResultBean() {
    }

    public ResultBean(List<Result> results) {
        this.results = results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public List<Result> getResults() {
        return results;
    }
}
