package ch.tocco.nice2.addressprovider.rest.api;

import java.util.Comparator;
import java.util.List;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

import ch.tocco.nice2.addressprovider.db.DatabaseConnector;
import ch.tocco.nice2.addressprovider.rest.error.BadRequestException;
import ch.tocco.nice2.addressprovider.strategy.AddressStrategy;
import ch.tocco.nice2.addressprovider.strategy.UniservAddressStrategy;

@RestController
@RequestMapping(path = "rest/v2/locations")
@ApiResponses(value = {
        @ApiResponse(responseCode = "401", description = "Unauthorized: Use http basic authentication"),
        @ApiResponse(responseCode = "500", description = "Internal Server Error")
})
@Tag(name = "Address Controller")
public class AddressController {

    private static final String PROVIDER_AUTH_TOKEN_HEADER_NAME = "X-Provider-Auth-Token";

    private final Logger logger = LoggerFactory.getLogger(AddressController.class);

    private final DatabaseConnector databaseConnector;

    private List<AddressStrategy> addressStrategies;

    @Autowired
    public AddressController(DatabaseConnector databaseConnector,
                             List<AddressStrategy> addressStrategies) {
        this.databaseConnector = databaseConnector;
        this.addressStrategies = addressStrategies;
    }

    @RequestMapping(path = "/search", method = RequestMethod.GET, produces = {"application/json"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "400", description = "Bad Request: Either postcodeStartsWith or cityStartsWith must be set and at least one country must be passed")
    })
    @Operation(
            summary = "Search locations",
            description = "Search locations with a name and/or postcode in a list of countries"
    )
    public ResultBean search(
            @Parameter(description = "first characters of postcode")
            @RequestParam(required = false)
            String postcodeStartsWith,
            @Parameter(description = "first characters of city name")
            @RequestParam(required = false)
            String cityStartsWith,
            @Parameter(description = "street to match")
            @RequestParam(required = false)
            String street,
            @Parameter(description = "house nr to match")
            @RequestParam(required = false)
            String houseNr,
            @Parameter(description = """
                    country iso codes, the country parameter may occur more than once,
                    e.g. country=DE&country=LI&country=CH (note: for Uniserv only one country is taken into account)
                    """
            )
            @RequestParam(required = false, name = "country")
            List<String> countries,
            @Parameter(description = "provider to use")
            @RequestParam(required = false)
            String provider,
            @RequestHeader(name = PROVIDER_AUTH_TOKEN_HEADER_NAME, required = false)
            String providerAuthToken
    ) {
        if (countries == null || countries.isEmpty()) {
            throw new BadRequestException("Query param country must be set");
        }

        if (Strings.isNullOrEmpty(postcodeStartsWith)
                && Strings.isNullOrEmpty(cityStartsWith)
                && Strings.isNullOrEmpty(street)) {
            throw new BadRequestException("Either postcodeStartsWith, cityStartsWith or street must be set");
        }

        postcodeStartsWith = Strings.nullToEmpty(postcodeStartsWith);
        cityStartsWith = Strings.nullToEmpty(cityStartsWith);

        // If provider "uniserv" is requested, only one country should be supplied (we only use one)
        // to avoid multiple requests (and thus expensive invoices).
        if (UniservAddressStrategy.PROVIDER_NAME.equals(provider)) {
            countries = List.of(getCountry(countries));
        }


        List<Result> results = getResults(
                postcodeStartsWith, cityStartsWith, street, houseNr, countries, provider, providerAuthToken
        );

        return new ResultBean(results);
    }

    private List<Result> getResults(String postcodeStartsWith, String cityStartsWith, String street, String houseNr, List<String> countries, String provider, String providerAuthToken) {
        List<Result> allResults = Lists.newArrayList();

        // make sure to return the results in the order of the address strategies (first the results of the more
        // important address strategies) and let every country only be handled by one strategy
        // -> loop over (remaining) countries in each address strategy, not the other way round

        List<String> unhandledCountries = Lists.newArrayList(countries);

        for (AddressStrategy addressStrategy : addressStrategies) {
            List<String> countriesHandledByStrategy = Lists.newArrayList();
            List<Result> resultsOfStrategy = Lists.newArrayList();

            for (String country : unhandledCountries) {
                if (addressStrategy.canHandle(postcodeStartsWith, cityStartsWith, street, houseNr, country, provider)) {
                    List<Result> results = addressStrategy.search(postcodeStartsWith, cityStartsWith, street, houseNr, country, provider, providerAuthToken);
                    resultsOfStrategy.addAll(results);
                    countriesHandledByStrategy.add(country);
                }
            }

            // if postcode/city search request, order the results like they were in older versions
            // if it's a (new) street request, don't change the order of the results (as already optimized by best
            // match by the service used in the strategies)
            if (Strings.isNullOrEmpty(street)) {
                resultsOfStrategy.sort(Comparator
                        .comparing(Result::getCity)
                        .thenComparing(Result::getPostcode));
            }

            allResults.addAll(resultsOfStrategy);

            unhandledCountries.removeAll(countriesHandledByStrategy);
        }

        return allResults;
    }

    @RequestMapping(path = "/validate", method = RequestMethod.POST, produces = {"application/json"})
    @Operation(
            summary = "Validate location",
            description = "Validate location and populate additional fields (e.g. municipality info)"
    )
    public Result validate(
            @Parameter(description = "provider to use")
            @RequestParam(required = false)
            String provider,
            @RequestParam(required = false, defaultValue = "false")
            boolean includeCoordinates,
            @RequestHeader(name = PROVIDER_AUTH_TOKEN_HEADER_NAME, required = false)
            String providerAuthToken,
            @RequestBody
            Result location
    ) {
        for (AddressStrategy addressStrategy : addressStrategies) {
            if (addressStrategy.canValidate(location, provider)) {
                return addressStrategy.validate(location, provider, providerAuthToken, includeCoordinates);
            }
        }
        throw new BadRequestException("Validate request could not be handled");
    }

    @Operation(
            summary = "Load all locations from the database",
            description = "Load all locations from the database"
    )
    @RequestMapping(method = RequestMethod.GET, produces = {"application/json"})
    public ResultBean getAll() {
        return new ResultBean(databaseConnector.getAll());
    }

    private String getCountry(@Nullable List<String> countries) {
        if (countries.size() == 1) {
            return countries.get(0);
        }

        logger.warn("Locations searched with more than 1 query parameter - only the first country will be used (or CH)!");

        // countries params list is sorted alphabetically - we use 1) CH 2) DE or otherwise the first one in the list
        if (countries.contains("CH")) {
            return "CH";
        }

        if (countries.contains("DE")) {
            return "DE";
        }

        return countries.get(0);
    }
}
