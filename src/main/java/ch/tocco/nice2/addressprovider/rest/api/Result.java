package ch.tocco.nice2.addressprovider.rest.api;

import java.math.BigDecimal;
import java.util.Objects;

import com.google.common.base.MoreObjects;

public class Result {

    private String street;
    private String houseNr;
    private String city;
    private String postcode;
    private String state;
    private String district;
    private String communityNr;
    private String country;
    private String postcodeAdditionalNr;

    private BigDecimal latitude;
    private BigDecimal longitude;

    /**
     * Set to true to request a validation request from the client when this result gets selected
     */
    private boolean validationRequired;

    public Result() {
    }

    public Result(
            String street,
            String houseNr,
            String city,
            String postcode,
            String state,
            String district,
            String communityNr,
            String country,
            String postcodeAdditionalNr,
            BigDecimal latitude,
            BigDecimal longitude
    ) {
        this.street = street;
        this.houseNr = houseNr;
        this.city = city;
        this.postcode = postcode;
        this.state = state;
        this.district = district;
        this.communityNr = communityNr;
        this.country = country;
        this.postcodeAdditionalNr = postcodeAdditionalNr;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNr() {
        return houseNr;
    }

    public void setHouseNr(String houseNr) {
        this.houseNr = houseNr;
    }

    public String getCity() {
        return city;
    }

    public String getPostcode() {
        return postcode;
    }

    public String getState() {
        return state;
    }

    public String getDistrict() {
        return district;
    }

    public String getCommunityNr() {
        return communityNr;
    }

    public String getCountry() {
        return country;
    }

    public String getPostcodeAdditionalNr() {
        return postcodeAdditionalNr;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public void setCommunityNr(String communityNr) {
        this.communityNr = communityNr;
    }

    public boolean isValidationRequired() {
        return validationRequired;
    }

    public void setValidationRequired(boolean validationRequired) {
        this.validationRequired = validationRequired;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Result result = (Result) o;
        return Objects.equals(street, result.street) &&
                Objects.equals(city, result.city) &&
                Objects.equals(postcode, result.postcode) &&
                Objects.equals(state, result.state) &&
                Objects.equals(district, result.district) &&
                Objects.equals(communityNr, result.communityNr) &&
                Objects.equals(country, result.country) &&
                Objects.equals(postcodeAdditionalNr, result.postcodeAdditionalNr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(street, city, postcode, state, district, communityNr, country, postcodeAdditionalNr);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("street", street)
                .add("city", city)
                .add("postcode", postcode)
                .add("state", state)
                .add("district", district)
                .add("communityNr", communityNr)
                .add("country", country)
                .add("postcodeAdditionalNr", postcodeAdditionalNr)
                .add("latitude", latitude)
                .add("longitude", longitude)
                .toString();
    }
}
