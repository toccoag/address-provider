package ch.tocco.nice2.addressprovider.rest.swagger;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {
    @Bean
    public OpenAPI api() {
        return new OpenAPI().info(
                new Info().title("SpringShop API")
                        .contact(new Contact().name("Tocco AG").url("https://www.tocco.ch/").email("support@tocco.ch"))
        );
    }
}
