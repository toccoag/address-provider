package ch.tocco.nice2.addressprovider.rest.api;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Deprecated since nice version 3.5 the v2 route is used. The difference to v1 is that latitude, longitude and
 * communityNr are additionally returned
 */
@Deprecated()
@RestController
@RequestMapping(path = "rest/v1/locations")
@ApiResponses(value = {
        @ApiResponse(responseCode = "401", description = "Unauthorized: Use http basic authentication"),
        @ApiResponse(responseCode = "500", description = "Internal Server Error")
})
@Tag(name = "Address Controller")
public class AddressControllerV1 {

    private final AddressController addressController;

    public AddressControllerV1(AddressController addressController) {
        this.addressController = addressController;
    }

    @RequestMapping(path = "/search", method = RequestMethod.GET, produces = {"application/json"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "400", description = "Bad Request: Either postcodeStartsWith or cityStartsWith must be set and at least one country must be passed")
    })
    @Operation(
            summary = "Search locations",
            description = "Search locations with a name and/or postcode in a list of countries"
    )
    public ResultBeanV1 search(@Parameter(description = "first characters of postcode") @RequestParam(required = false) String postcodeStartsWith,
                               @Parameter(description = "first characters of city name") @RequestParam(required = false) String cityStartsWith,
                               @Parameter(description = "country iso codes, the country parameter may occur more than once, e.g. country=DE&country=LI&country=CH") @RequestParam(required = false, name = "country") List<String> countries) {
        return convert(addressController.search(postcodeStartsWith, cityStartsWith, null, null, countries, null, null));
    }

    @Operation(
            summary = "Load all locations from the database",
            description = "Load all locations from the database"
    )
    @RequestMapping(method = RequestMethod.GET, produces = {"application/json"})
    public ResultBeanV1 getAll() {
        return convert(addressController.getAll());
    }

    private ResultBeanV1 convert(ResultBean bean) {
        List<ResultV1> results = bean.getResults()
                .stream()
                .map(ResultV1::new)
                .toList();
        return new ResultBeanV1(results);
    }

    public static class ResultBeanV1 {

        private List<ResultV1> results;

        public ResultBeanV1() {
        }

        public ResultBeanV1(List<ResultV1> results) {
            this.results = results;
        }

        public void setResults(List<ResultV1> results) {
            this.results = results;
        }

        public List<ResultV1> getResults() {
            return results;
        }
    }

    public static class ResultV1 extends Result {

        public ResultV1(Result result) {
            super(null, null, result.getCity(), result.getPostcode(), result.getState(), result.getDistrict(), result.getCommunityNr(), result.getCountry(), result.getPostcodeAdditionalNr(), result.getLatitude(), result.getLongitude());
        }

        @Override
        @JsonIgnore
        public String getCommunityNr() {
            return super.getCommunityNr();
        }

        @Override
        @JsonIgnore
        public BigDecimal getLatitude() {
            return super.getLatitude();
        }

        @Override
        @JsonIgnore
        public BigDecimal getLongitude() {
            return super.getLongitude();
        }

        @Override
        @JsonIgnore
        public String getStreet() {
            return super.getStreet();
        }

        @Override
        @JsonIgnore
        public String getHouseNr() {
            return super.getHouseNr();
        }

        @Override
        @JsonIgnore
        public boolean isValidationRequired() {
            return super.isValidationRequired();
        }
    }
}
