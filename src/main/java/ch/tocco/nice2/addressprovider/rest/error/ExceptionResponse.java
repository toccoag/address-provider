package ch.tocco.nice2.addressprovider.rest.error;

public class ExceptionResponse {

    private int status;
    private String message;

    public ExceptionResponse() {
    }

    public ExceptionResponse(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
