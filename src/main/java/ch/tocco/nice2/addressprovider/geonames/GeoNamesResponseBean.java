package ch.tocco.nice2.addressprovider.geonames;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import ch.tocco.nice2.addressprovider.rest.api.Result;

public class GeoNamesResponseBean {
    private List<GeoNamesLocationBean> postalCodes = Collections.emptyList();

    public List<GeoNamesLocationBean> getPostalCodes() {
        return postalCodes;
    }

    public void setPostalCodes(List<GeoNamesLocationBean> postalCodes) {
        this.postalCodes = postalCodes;
    }

    public List<Result> toResults() {
        return postalCodes.stream().map(GeoNamesLocationBean::toResult).collect(Collectors.toList());
    }
}
