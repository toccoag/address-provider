package ch.tocco.nice2.addressprovider.geonames;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ch.tocco.nice2.addressprovider.rest.api.Result;

@JsonIgnoreProperties({"ISO3166-2", "adminCode2", "adminCode3", "adminName1"})
public class GeoNamesLocationBean {
    private String adminCode1;
    private String adminName2;
    private String countryCode;
    private String placeName;
    private String postalCode;

    private BigDecimal lat;
    private BigDecimal lng;

    public GeoNamesLocationBean() {
    }

    public GeoNamesLocationBean(String adminCode1, String adminName2, String countryCode, String placeName, String postalCode) {
        this.adminCode1 = adminCode1;
        this.adminName2 = adminName2;
        this.countryCode = countryCode;
        this.placeName = placeName;
        this.postalCode = postalCode;
    }

    public String getAdminCode1() {
        return adminCode1;
    }

    public void setAdminCode1(String adminCode1) {
        this.adminCode1 = adminCode1;
    }

    public String getAdminName2() {
        return adminName2;
    }

    public void setAdminName2(String adminName2) {
        this.adminName2 = adminName2;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public BigDecimal getLat() {
        return lat;
    }

    public void setLat(BigDecimal lat) {
        this.lat = lat;
    }

    public BigDecimal getLng() {
        return lng;
    }

    public void setLng(BigDecimal lng) {
        this.lng = lng;
    }

    public Result toResult() {
        return new Result(null, null, placeName, postalCode, adminCode1, adminName2, null, countryCode, null, lat, lng);
    }
}
