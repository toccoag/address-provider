package ch.tocco.nice2.addressprovider.geonames;

import java.util.List;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import ch.tocco.nice2.addressprovider.rest.api.Result;

@Component
public class GeonamesConnector {

    private final Logger logger = LoggerFactory.getLogger(GeonamesConnector.class);

    private final RestTemplate restTemplate;

    @Value("${geonames.api.url}")
    private String url;

    @Value("${geonames.api.user}")
    private String user;

    @Value("${geonames.api.token}")
    private String token;

    public GeonamesConnector(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public List<Result> search(String postcodeStartsWith, String cityStartsWith, List<String> countries) {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url)
                .queryParam("username", user)
                .queryParam("token", token);

        countries.forEach(country -> uriBuilder.queryParam("country", country));

        if (!Strings.isNullOrEmpty(postcodeStartsWith)) {
            uriBuilder.queryParam("postalcode_startsWith", postcodeStartsWith);
        }

        if (!Strings.isNullOrEmpty(cityStartsWith)) {
            uriBuilder.queryParam("placename_startsWith", cityStartsWith);
        }

        List<Result> results = Lists.newArrayList();
        try {
            GeoNamesResponseBean responseBean = restTemplate.getForObject(uriBuilder.build(false).toUriString(), GeoNamesResponseBean.class);
            results = responseBean.toResults();
        } catch(HttpServerErrorException | ResourceAccessException errorException) {
            logger.error("Invalid response of Geonames: " + errorException.getMessage());
        }

        results.sort((Result r1, Result r2) -> {
            if (r1.getCity().equals(r2.getCity())) {
                return r1.getPostcode().compareTo(r2.getPostcode());
            } else {
                return r1.getCity().compareTo(r2.getCity());
            }
        });

        return results;
    }
}
