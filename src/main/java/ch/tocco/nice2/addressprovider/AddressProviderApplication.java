package ch.tocco.nice2.addressprovider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@PropertySource(value = "application.local.properties", ignoreResourceNotFound = true)
@EnableWebMvc
public class AddressProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(AddressProviderApplication.class, args);
    }
}
