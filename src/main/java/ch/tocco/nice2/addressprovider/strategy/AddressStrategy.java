package ch.tocco.nice2.addressprovider.strategy;

import java.util.List;

import ch.tocco.nice2.addressprovider.rest.api.Result;

/**
 * Depending on the input params, the strategy to obtain the results might be different.
 * The implementations of this interface handle all the different cases.
 */
public interface AddressStrategy {

    boolean canHandle(
            String postcodeStartsWith,
            String cityStartsWith,
            String street,
            String houseNr,
            String country,
            String provider
    );

    List<Result> search(
            String postcodeStartsWith,
            String cityStartsWith,
            String street,
            String houseNr,
            String country,
            String provider,
            String providerAuthToken
    );

    default boolean canValidate(Result location, String provider) {
        return false;
    }

    default Result validate(Result location,
                    String provider,
                    String providerAuthToken,
                    boolean includeCoordinates) {
        throw new UnsupportedOperationException("Validate not implemented");
    }
}
