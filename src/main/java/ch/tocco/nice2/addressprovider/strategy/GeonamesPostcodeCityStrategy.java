package ch.tocco.nice2.addressprovider.strategy;

import java.util.List;

import com.google.common.base.Strings;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import ch.tocco.nice2.addressprovider.geonames.GeonamesConnector;
import ch.tocco.nice2.addressprovider.rest.api.Result;

/**
 * This strategy is used for all postcode/city requests (no full address requests with street) for all countries
 * (as the last strategy).
 */
@Component
@Order(4)
public class GeonamesPostcodeCityStrategy implements AddressStrategy {

    private final GeonamesConnector geonamesConnector;

    public GeonamesPostcodeCityStrategy(GeonamesConnector geonamesConnector) {
        this.geonamesConnector = geonamesConnector;
    }

    @Override
    public boolean canHandle(String postcodeStartsWith,
                             String cityStartsWith,
                             String street,
                             String houseNr,
                             String country,
                             String provider) {
        return Strings.isNullOrEmpty(street);
    }

    @Override
    public List<Result> search(String postcodeStartsWith,
                               String cityStartsWith,
                               String street,
                               String houseNr,
                               String country,
                               String provider,
                               String providerAuthToken) {
        return geonamesConnector.search(postcodeStartsWith, cityStartsWith, List.of(country));
    }
}
