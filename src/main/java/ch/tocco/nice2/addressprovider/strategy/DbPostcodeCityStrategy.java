package ch.tocco.nice2.addressprovider.strategy;

import java.util.List;

import com.google.common.base.Strings;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import ch.tocco.nice2.addressprovider.db.DatabaseConnector;
import ch.tocco.nice2.addressprovider.rest.api.Result;

/**
 * This strategy is used for postcode/city requests (without street) if the database contains the
 * requested country (currently this is the case for CH only).
 */
@Component
@Order(3)
public class DbPostcodeCityStrategy implements AddressStrategy {

    private final DatabaseConnector databaseConnector;

    public DbPostcodeCityStrategy(DatabaseConnector databaseConnector) {
        this.databaseConnector = databaseConnector;
    }

    @Override
    public boolean canHandle(String postcodeStartsWith,
                             String cityStartsWith,
                             String street,
                             String houseNr,
                             String country,
                             String provider) {
        return Strings.isNullOrEmpty(street) && databaseConnector.getSupportedCountries().contains(country);
    }

    @Override
    public List<Result> search(String postcodeStartsWith,
                               String cityStartsWith,
                               String street,
                               String houseNr,
                               String country,
                               String provider,
                               String providerAuthToken) {
        return databaseConnector.search(postcodeStartsWith, cityStartsWith, List.of(country));
    }
}
