package ch.tocco.nice2.addressprovider.strategy;

import java.util.List;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import ch.tocco.nice2.addressprovider.rest.api.Result;
import ch.tocco.nice2.addressprovider.uniserv.UniservConnector;

/**
 * If "uniserv" is selected as provider, we use this strategy for all requests (all countries,
 * requests for whole addresses including street and also for postcode/city only).
 */
@Component
@Order(1)
public class UniservAddressStrategy implements AddressStrategy {

    public static final String PROVIDER_NAME = "uniserv";

    private final UniservConnector uniservConnector;

    public UniservAddressStrategy(UniservConnector uniservConnector) {
        this.uniservConnector = uniservConnector;
    }

    @Override
    public boolean canHandle(String postcodeStartsWith,
                             String cityStartsWith,
                             String street,
                             String houseNr,
                             String country,
                             String provider) {
        return PROVIDER_NAME.equals(provider);
    }

    @Override
    public List<Result> search(String postcodeStartsWith,
                               String cityStartsWith,
                               String street,
                               String houseNr,
                               String country,
                               String provider,
                               String providerAuthToken) {
        return uniservConnector.search(providerAuthToken, postcodeStartsWith, cityStartsWith, street, houseNr, country);
    }

    @Override
    public boolean canValidate(Result location, String provider) {
        return PROVIDER_NAME.equals(provider);
    }

    @Override
    public Result validate(Result location,
                           String provider,
                           String providerAuthToken,
                           boolean includeCoordinates) {
        return uniservConnector.validate(providerAuthToken, location, includeCoordinates);
    }
}
