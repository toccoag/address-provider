package ch.tocco.nice2.addressprovider.strategy;

import java.util.List;

import com.google.common.base.Strings;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import ch.tocco.nice2.addressprovider.db.DatabaseConnector;
import ch.tocco.nice2.addressprovider.rest.api.Result;
import ch.tocco.nice2.addressprovider.swisspost.SwissPostConnector;

/**
 * If the country is Switzerland (CH), we use this strategy for address searches with streets.
 * We don't use it for searches for only postcode/city -> those are handled by the {@link DbPostcodeCityStrategy}.
 */
@Component
@Order(2)
public class SwissPostAddressStrategy implements AddressStrategy {

    private final DatabaseConnector databaseConnector;
    private final SwissPostConnector swissPostConnector;

    public SwissPostAddressStrategy(DatabaseConnector databaseConnector,
                                    SwissPostConnector swissPostConnector) {
        this.databaseConnector = databaseConnector;
        this.swissPostConnector = swissPostConnector;
    }

    @Override
    public boolean canHandle(String postcodeStartsWith,
                             String cityStartsWith,
                             String street,
                             String houseNr,
                             String country,
                             String provider) {
        return "CH".equals(country) && !Strings.isNullOrEmpty(street);
    }

    @Override
    public List<Result> search(String postcodeStartsWith,
                               String cityStartsWith,
                               String street,
                               String houseNr,
                               String country,
                               String provider,
                               String providerAuthToken) {
        if (houseNr == null) {
            houseNr = "";
        }
        if (street == null) {
            street = "";
        }
        List<Result> results = swissPostConnector.search(postcodeStartsWith, cityStartsWith, street, houseNr);
        databaseConnector.supplementData(results);
        return results;
    }
}
