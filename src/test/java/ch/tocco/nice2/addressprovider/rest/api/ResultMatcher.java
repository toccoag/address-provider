package ch.tocco.nice2.addressprovider.rest.api;

import java.util.Objects;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

public class ResultMatcher extends BaseMatcher<Result> {

    private final String city;
    private final String postcode;
    private final String state;
    private final String district;
    private final String country;
    private final String postcodeAdditionalNr;

    public static ResultMatcher isResult(Result result) {
        return new ResultMatcher(result.getCity(),
                result.getPostcode(),
                result.getState(),
                result.getDistrict(),
                result.getCountry(),
                result.getPostcodeAdditionalNr());
    }

    public ResultMatcher(String city, String postcode, String state, String district, String country, String postcodeAdditionalNr) {
        this.city = city;
        this.postcode = postcode;
        this.state = state;
        this.district = district;
        this.country = country;
        this.postcodeAdditionalNr = postcodeAdditionalNr;
    }

    @Override
    public boolean matches(Object item) {
        if (item instanceof Result) {
            Result result = (Result) item;
            return Objects.equals(city, result.getCity()) && Objects.equals(postcode, result.getPostcode())
                    && Objects.equals(state, result.getState()) && Objects.equals(district, result.getDistrict())
                    && Objects.equals(country, result.getCountry()) && Objects.equals(postcodeAdditionalNr, result.getPostcodeAdditionalNr());
        }
        return false;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText(String.format("Result (%s, %s, %s, %s, %s, %s)", city, postcode, state, district, country, postcodeAdditionalNr));
    }
}
