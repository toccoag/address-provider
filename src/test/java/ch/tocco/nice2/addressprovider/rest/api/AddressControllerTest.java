package ch.tocco.nice2.addressprovider.rest.api;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import ch.tocco.nice2.addressprovider.AbstractDatabaseTest;
import ch.tocco.nice2.addressprovider.db.DatabaseConnector;
import ch.tocco.nice2.addressprovider.geonames.GeonamesConnector;
import ch.tocco.nice2.addressprovider.rest.error.ExceptionResponse;

import static ch.tocco.nice2.addressprovider.rest.api.ResultMatcher.isResult;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.Series.CLIENT_ERROR;
import static org.springframework.http.HttpStatus.Series.SERVER_ERROR;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class AddressControllerTest extends AbstractDatabaseTest {

    @LocalServerPort
    private int port;

    @Autowired
    private RestTemplate restClient;

    @MockitoBean
    private GeonamesConnector geonamesConnector;

    @MockitoBean
    private DatabaseConnector databaseConnector;

    private final Result resultDatabase1 = new Result(null, null, "Zürich", "8000", "ZH", "Bezirk Zürich", "261", "CH", "00", BigDecimal.valueOf(47.37), BigDecimal.valueOf(8.54));
    private final Result resultDatabase2 = new Result(null, null, "Bern", "3000", "BE", "Verwaltungskreis Bern-Mittelland", "351", "CH", "00", BigDecimal.valueOf(46.94), BigDecimal.valueOf(7.44));
    private final Result resultGeonames = new Result(null, null, "Eggersdorf bei Graz", "8063", "06", "Politischer Bezirk Graz-Umgebung", null, "AT", "00", BigDecimal.valueOf(47.12), BigDecimal.valueOf(15.58));

    @BeforeEach
    public void setupDatabaseConnector() {
        when(databaseConnector.getSupportedCountries()).thenReturn(List.of("CH"));
    }

    @Test
    public void searchDatabase() {
        when(databaseConnector.search(anyString(), anyString(), anyList())).thenReturn(List.of(resultDatabase1));

        List<Result> list = restClient.getForObject(url("/locations/search?country=CH&postcodeStartsWith=8"), ResultBean.class).getResults();
        assertThat(list, hasSize(1));
        assertThat(list, contains(isResult(resultDatabase1)));
    }

    @Test
    public void searchGeonames() {
        when(geonamesConnector.search(anyString(), anyString(), anyList())).thenReturn(List.of(resultGeonames));

        List<Result> list = restClient.getForObject(url("/locations/search?country=AT&postcodeStartsWith=8"), ResultBean.class).getResults();
        assertThat(list, hasSize(1));
        assertThat(list, contains(isResult(resultGeonames)));
    }

    @Test
    public void searchMultipleCountries() {
        when(databaseConnector.search(anyString(), anyString(), anyList())).thenReturn(List.of(resultDatabase1));
        when(geonamesConnector.search(anyString(), anyString(), anyList())).thenReturn(List.of(resultGeonames));

        List<Result> list = restClient.getForObject(url("/locations/search?country=CH&country=AT&postcodeStartsWith=8"), ResultBean.class).getResults();
        assertEquals(List.of(resultDatabase1, resultGeonames), list);
    }

    @Test
    public void missingSearchString() {
        ResponseEntity<ExceptionResponse> response = restClient.exchange(url("/locations/search?country=CH"), HttpMethod.GET, null, ExceptionResponse.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("Either postcodeStartsWith, cityStartsWith or street must be set", response.getBody().getMessage());
    }

    @Test
    public void missingCountry() {
        ResponseEntity<ExceptionResponse> response = restClient.exchange(url("/locations/search?postcodeStartsWith=8"), HttpMethod.GET, null, ExceptionResponse.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("Query param country must be set", response.getBody().getMessage());
    }

    @Test
    public void getAll() {
        when(databaseConnector.getAll()).thenReturn(List.of(resultDatabase2, resultDatabase1));

        List<Result> list = restClient.getForObject(url("/locations"), ResultBean.class).getResults();
        assertThat(list, hasSize(2));
        assertEquals(List.of(resultDatabase2, resultDatabase1), list);
    }

    @Test
    public void unauthorized() {
        RestTemplate restClient = new RestTemplateBuilder()
                .errorHandler(new RestResponseErrorHandler())
                .build();

        ResponseEntity<ResultBean> response = restClient.exchange(url("/locations/"), HttpMethod.GET, null, ResultBean.class);
        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
    }

    private String url(String path) {
        return String.format("http://localhost:%s/rest/v2%s", port, path);
    }

    @TestConfiguration
    static class RestTestConfiguration {

        @Value("${rest.api.username}")
        private String username;

        @Value("${rest.api.password}")
        private String password;

        @Bean("restClient")
        public RestTemplate restClient() {
            return new RestTemplateBuilder()
                    .basicAuthentication(username, password)
                    .errorHandler(new RestResponseErrorHandler())
                    .build();
        }
    }

    @Component
    static class RestResponseErrorHandler implements ResponseErrorHandler {

        @Override
        public boolean hasError(ClientHttpResponse httpResponse) throws IOException {
            return httpResponse.getStatusCode().is4xxClientError() || httpResponse.getStatusCode().is5xxServerError();
        }

    }
}
