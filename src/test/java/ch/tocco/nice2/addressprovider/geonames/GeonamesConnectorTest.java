package ch.tocco.nice2.addressprovider.geonames;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.RestTemplate;

import ch.tocco.nice2.addressprovider.rest.api.Result;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withServerError;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@SpringBootTest
@ActiveProfiles("test")
public class GeonamesConnectorTest {

    @Autowired
    private GeonamesConnector geonamesConnector;

    @Autowired
    private RestTemplate restTemplate;

    @Value("classpath:geonames.json")
    private Resource jsonResponse;

    private MockRestServiceServer mockServer;

    @BeforeEach
    public void setUp() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    @ParameterizedTest
    @MethodSource("searchData")
    public void search(String zipStartsWith, String cityStartsWith, List<String> countries, String uri) throws IOException {
        mockRequest(uri);
        List<Result> list = geonamesConnector.search(zipStartsWith, cityStartsWith, countries);
        assertThat(list, hasSize(5));

        mockServer.verify();
    }

    private static Stream<Arguments> searchData() {
        return Stream.of(
                arguments("8", "", List.of("AT"), "https://secure.geonames.net/postalCodeSearchJSON?username=test&token=password&country=AT&postalcode_startsWith=8"),
                arguments("", "Graz", List.of("AT"), "https://secure.geonames.net/postalCodeSearchJSON?username=test&token=password&country=AT&placename_startsWith=Graz"),
                arguments("", "Graz", List.of("AT", "DE"), "https://secure.geonames.net/postalCodeSearchJSON?username=test&token=password&country=AT&country=DE&placename_startsWith=Graz")
        );
    }

    @Test
    public void sorting() throws IOException {
        mockRequest("https://secure.geonames.net/postalCodeSearchJSON?username=test&token=password&country=AT&postalcode_startsWith=8063");
        List<Result> list = geonamesConnector.search("8063", "", List.of("AT"));

        assertThat(list, hasSize(5));
        List<String> expected = List.of("Edelsbach bei Graz", "Eggersdorf bei Graz", "Haselbach", "Höf", "Purgstall bei Eggersdorf");
        assertEquals(expected, list.stream().map(Result::getCity).collect(Collectors.toList()));
    }

    /**
     * Geonames API does not provide the community nr, therefore we always set it to null.
     *
     * What we mean by "community nr" is, for instance, the "Amtlicher Gemeindeschlüssel (AGS)" in Germany,
     * e.g. "13072009" for 18246 Baumgarten). This is not provided by the Geonames API.
     */
    @Test
    public void noCommunityNr() throws IOException {
        mockRequest("https://secure.geonames.net/postalCodeSearchJSON?username=test&token=password&country=AT&postalcode_startsWith=8063");
        List<Result> list = geonamesConnector.search("8063", "", List.of("AT"));

        assertThat(list, hasSize(5));
        list.forEach(result -> assertNull(result.getCommunityNr())); // geonames API does not
    }

    @Test
    public void serverNotAvailable() {
        mockRequestFailed("https://secure.geonames.net/postalCodeSearchJSON?username=test&token=password&country=AT&postalcode_startsWith=8063");
        List<Result> list = geonamesConnector.search("8063", "", List.of("AT"));
        assertThat(list, hasSize(0));
    }

    private void mockRequest(String uri) throws IOException {
        String response = StreamUtils.copyToString(jsonResponse.getInputStream(), Charset.forName("UTF-8"));
        mockServer.expect(requestTo(uri))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(response, MediaType.APPLICATION_JSON));
    }

    private void mockRequestFailed(String uri) {
        mockServer.expect(requestTo(uri))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withServerError());
    }
}
