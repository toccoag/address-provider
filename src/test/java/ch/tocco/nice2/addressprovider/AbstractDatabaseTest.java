package ch.tocco.nice2.addressprovider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;

public class AbstractDatabaseTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @BeforeEach
    public void resetDatabase() throws IOException {
        importFile("schema.sql");
        clearDatabase();
        importFile("data.sql");
    }

    public void clearDatabase() {
        jdbcTemplate.execute("DELETE FROM city;");
        jdbcTemplate.execute("DELETE FROM district;");
    }

    private void importFile(String file) throws IOException {
        InputStream inputStream = new ClassPathResource(file).getInputStream();
        String text = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))
                .lines()
                .collect(Collectors.joining("\n"));

        for (String stmt : Arrays.asList(text.split(";"))) {
            jdbcTemplate.execute(stmt);
        }
    }
}
