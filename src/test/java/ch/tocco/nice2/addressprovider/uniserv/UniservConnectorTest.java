package ch.tocco.nice2.addressprovider.uniserv;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import ch.tocco.nice2.addressprovider.rest.api.Result;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@SpringBootTest
@ActiveProfiles("test")
public class UniservConnectorTest {

    private static final String AUTH_TOKEN = "test-auth-token";

    private static final UniservValidationResponseBean.Coordinates TEST_COORDINATES =
            new UniservValidationResponseBean.Coordinates();
    static {
        UniservValidationResponseBean.WGS84 wgs84 = new UniservValidationResponseBean.WGS84();
        wgs84.setLat(47.389627495014636);
        wgs84.setLon(8.54151728021034);
        TEST_COORDINATES.setWgs84(wgs84);
    }

    @Autowired
    private UniservConnector uniservConnector;
    @Autowired
    private RestTemplate restTemplate;

    private MockRestServiceServer mockServer;

    @BeforeEach
    public void setUp() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
        uniservConnector.clearSessionTokens();
    }

    @ParameterizedTest
    @MethodSource("validateData")
    public void testValidateWithCoordinates(boolean includeCoordinates) throws Exception {
        SessionTokenRequestBean expectedSessionTokenRequestBean = new SessionTokenRequestBean("ch");

        SessionTokenBean sessionTokenBean = new SessionTokenBean();
        sessionTokenBean.setSessionToken("session-token");

        ValidateRequestBean expectedValidateRequestBean = new ValidateRequestBean("ch", "8006", "Zürich", "Riedtlistrasse", "27", includeCoordinates);

        UniservValidationResponseBean.ValidationResult validationResult = new UniservValidationResponseBean.ValidationResult();
        UniservValidationResponseBean validateResponseBean = new UniservValidationResponseBean();
        if (Arrays.asList(expectedValidateRequestBean.getOptions().getInclude()).contains("coordinates-wgs84")) {
            validationResult.setCoordinates(TEST_COORDINATES);
        }
        validateResponseBean.setResults(List.of(validationResult));

        mockRequest(
                "https://api.uniserv.com/location/v1/start-session",
                expectedSessionTokenRequestBean,
                sessionTokenBean
        );

        mockRequest(
                "https://api.uniserv.com/location/v1/validate",
                expectedValidateRequestBean,
                validateResponseBean
        );

        Result location = new Result("Riedtlistrasse", "27", "Zürich", "8006", null, null, null, "ch", null, null, null);
        Result result = uniservConnector.validate(AUTH_TOKEN, location, includeCoordinates);

        if (includeCoordinates) {
            assertEquals(result.getLatitude(), BigDecimal.valueOf(TEST_COORDINATES.getWgs84().getLat()));
            assertEquals(result.getLongitude(), BigDecimal.valueOf(TEST_COORDINATES.getWgs84().getLon()));
        } else {
            assertNull(result.getLatitude());
            assertNull(result.getLongitude());
        }

        mockServer.verify();
    }

    private static Stream<Arguments> validateData() {
        return Stream.of(
                arguments(false),
                arguments(true)
        );
    }

    private void mockRequest(String uri, Object requestBean, Object responseBean) throws IOException {
        String requestJson = new ObjectMapper().writeValueAsString(requestBean);
        String responseJson = new ObjectMapper().writeValueAsString(responseBean);

        mockServer.expect(requestTo(uri))
                .andExpect(content().json(requestJson))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(responseJson, MediaType.APPLICATION_JSON));
    }
}
