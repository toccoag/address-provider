package ch.tocco.nice2.addressprovider.db;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import ch.tocco.nice2.addressprovider.AbstractDatabaseTest;
import ch.tocco.nice2.addressprovider.rest.api.Result;

import static ch.tocco.nice2.addressprovider.rest.api.ResultMatcher.isResult;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest
@ActiveProfiles("test")
public class DatabaseConnectorTest extends AbstractDatabaseTest {

    @Autowired
    private DatabaseConnector databaseConnector;

    @Test
    public void postcodeStartsWith() {
        List<Result> list = databaseConnector.search("87", "", List.of("CH"));
        assertEquals(List.of("Männedorf", "Stäfa"), list.stream().map(Result::getCity).collect(Collectors.toList()));
    }

    @Test
    public void cityStartsWith() {
        // case insensitive
        List<Result> list = databaseConnector.search("", "zÜrIC", List.of("CH"));
        assertEquals(List.of("Zürich"), list.stream().map(Result::getCity).collect(Collectors.toList()));
        Result expected = new Result(null, null, "Zürich", "8000", "ZH", "Bezirk Zürich", "261", "CH", "00", BigDecimal.valueOf(47.37), BigDecimal.valueOf(8.54));
        assertThat(list, contains(isResult(expected)));
    }

    @Test
    public void noResult() {
        List<Result> list = databaseConnector.search("87", "Z", List.of("CH"));
        assertEquals(List.of(), list.stream().map(Result::getCity).collect(Collectors.toList()));
    }

    @Test
    public void otherCountry() {
        List<Result> list = databaseConnector.search("8", "", List.of("LI"));
        assertEquals(List.of(), list.stream().map(Result::getCity).collect(Collectors.toList()));
    }

    @Test
    public void multipleCountries() {
        List<Result> list = databaseConnector.search("80", "", List.of("LI", "CH"));
        assertEquals(List.of("Zürich"), list.stream().map(Result::getCity).collect(Collectors.toList()));
    }

    @Test
    public void limit() {
        List<Result> list = databaseConnector.search("8", "", List.of("CH"));
        assertEquals(List.of("Männedorf", "Stäfa"), list.stream().map(Result::getCity).collect(Collectors.toList()));
    }

    @Test
    public void noDistrict() {
        List<Result> list = databaseConnector.search("", "Männedorf", List.of("CH"));
        assertNull(list.get(0).getDistrict());
    }

    @Test
    public void getAll() {
        List<Result> list = databaseConnector.getAll();
        assertThat(list, hasSize(3));
    }
}
