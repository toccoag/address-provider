INSERT INTO district (nr, name) VALUES(107, 'Bezirk Meilen');
INSERT INTO district (nr, name) VALUES(112, 'Bezirk Zürich');

INSERT INTO city (iso_code, zip, zip_additional_nr, name, search_term, canton, district_nr, community_nr, latitude, longitude) VALUES('CH', '8712', '00', 'Stäfa', 'STÄFA', 'ZH', 107, '158', 47.24, 8.725);
INSERT INTO city (iso_code, zip, zip_additional_nr, name, search_term, canton, district_nr, community_nr, latitude, longitude) VALUES('CH', '8708', '00', 'Männedorf', 'MÄNNEDORF', 'ZH', null, '155', 47.25, 8.69);
INSERT INTO city (iso_code, zip, zip_additional_nr, name, search_term, canton, district_nr, community_nr, latitude, longitude) VALUES('CH', '8000', '00', 'Zürich', 'ZÜRICH', 'ZH', 112, '261', 47.37, 8.54);
