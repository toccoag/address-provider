#!/usr/bin/python3
import os
import subprocess

with open('/etc/passwd', 'r+') as f:
    f.write('user:x:{}:{}:,,,:/home/user:/bin/sh\n'.format(os.getuid(), os.getgid()))

subprocess.call(['java', '-jar', 'address-provider-0.0.1-SNAPSHOT.jar'])
