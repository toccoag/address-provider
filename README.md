# Address Provider

## Getting started

**Build:**
```
./gradlew build
```

**Add a local config file:**

Config file location: `src/main/resources/application-dev.properties`:

Required properties in new file:
```
spring.main.allow-bean-definition-overriding=true

spring.datasource.url=jdbc:sqlite:test.db

rest.api.username=****
rest.api.password=****
geonames.api.token=****
swisspost.api.username=*****
swisspost.api.password=****
```

`geonames.api.token` is in the ansible vault. As an alternative you can use a non-premium account using:
```
geonames.api.url=http://api.geonames.org/postalCodeSearchJSON
geonames.api.user=tocco
geonames.api.token=
```

Also, `swisspost.api.username` and `swisspost.api.password` can be found in the ansible vault.

**Setup database:**

Navigate to the root folder of this repo and import the database schema:
```
sqlite3 test.db < database/schema.sql 
```

Open the sqlite3 cli with `sqlite3 test.db` and import the data:

```
.mode csv
.import database/district.csv district
.import database/city.csv city
```

**Start application:**

Run `ch.tocco.nice2.addressprovider.AddressProviderApplication` with 
VM option `-Dspring.profiles.active=dev`

**Swagger:**

- local: http://localhost:8080/swagger-ui/index.html (username is `rest.api.username` & password is `rest.api.password`)
- test: https://address-provider-test.tocco.ch/swagger-ui/index.html (username: tocco & password see ansible vault)
- production: https://address-provider.tocco.ch/swagger-ui/index.html (username: tocco & password see ansible vault)

## Update data source

1. Download the Post csv file from https://service.post.ch/zopa/dlc/app/?lang=de&service=dlc-web#/main (free post login is needed) and copy it into the root folder of this repo. This dataset is used for zip code part.
2. Download the BfS/swisstopo csv file `ortschaftenverzeichnis_plz_4326.csv.zip` from https://www.swisstopo.admin.ch/de/amtliches-ortschaftenverzeichnis#Ortschaftenverzeichnis--Download and copy the csv file into the root folder of this repo. This dataset is used for coordinates.
3. Run the python script with `python3 importer.py CSV_FILE_BFS CSV_FILE_POST` (Install the needed dependencies with `pip install pandas openpyxl` or `apt install python3-openpyxl python3-pandas`)
4. Commit the updated csv files
