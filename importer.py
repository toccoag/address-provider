#!/usr/bin/python3

import math
import io
import pandas as pd
import sys
from datetime import datetime

CANTONS = ['AG', 'AI', 'AR', 'BE', 'BL', 'BS', 'FR', 'GE', 'GL', 'GR', 'JU', 'LU', 'NE', 'NW', 'OW', 'SG', 'SH', 'SO',
           'SZ', 'TG', 'TI', 'UR', 'VD', 'VS', 'ZG', 'ZH']
ALLOWED_ZIP_TYPES = [10, 20]
# the BfS dataset does not contain coordinates of large cities (e.g. 8000),
# coordinates must be manually set (copied from Wikipedia)
COORDINATES = {
    # large cities
    1000: {'latitude': 46.51942, 'longitude': 6.63448},
    1200: {'latitude': 46.200013, 'longitude': 6.149985},
    2500: {'latitude': 47.13679, 'longitude': 7.24674},
    3000: {'latitude': 46.94798, 'longitude': 7.44743},
    4000: {'latitude': 47.55814, 'longitude': 7.58769},
    6000: {'latitude': 47.05207, 'longitude': 8.30585},
    8000: {'latitude': 47.37174, 'longitude': 8.54226},
    # Others
    2345: {'latitude': 47.197657181045194, 'longitude': 6.963725486151355},
    6777: {'latitude': 46.51340, 'longitude': 8.72439}
}

if len(sys.argv) != 3:
    raise AssertionError('Pass csv file names as argument (python3 import.py CSV_FILE_BFS CSV_FILE_POST)')

csvFileBfs = sys.argv[1]
csvFilePost = sys.argv[2]

bfsDate = datetime.today().strftime('%d-%m-%Y')
dfBfsLevels = pd.read_csv(f'https://www.agvchapp.bfs.admin.ch/api/communes/levels?date={bfsDate}')

dfDistrict = dfBfsLevels[['DistrictId','District']].drop_duplicates()
dfDistrict.columns = ['nr', 'name']
dfDistrict = dfDistrict.sort_values(by='nr')
dfDistrict.to_csv('database/district.csv', index=False, header=False)

dfCityDistrict = dfBfsLevels[['DistrictId', 'BfsCode']]
dfCityDistrict.columns = ['district_nr', 'community_nr']
dfCityCoordinate = pd.read_csv(csvFileBfs, sep=';', usecols=['PLZ', 'Zusatzziffer', 'E', 'N'])
dfCityCoordinate.columns = ['zip', 'zip_additional_nr', 'longitude', 'latitude']
# for some zip/zip_additional_nr multiple rows exist, just use first one
dfCityCoordinate = dfCityCoordinate.drop_duplicates(subset=['zip', 'zip_additional_nr'])

# the csv file of the post contains multiple tables. the first column of the csv indicates the table name
# we only need the table "01" so filter out the other ones
filteredLines = ''
with open(csvFilePost, 'r', encoding='cp1252') as file:
    for line in file:
        if line.startswith('01;'):
            filteredLines += line
# Mapping of columns
# - BFSNR -> community_nr
# - PLZ_TYP -> zip_type
# - POSTLEITZAHL -> zip
# - PLZ_ZZ -> zip_additional_nr
# - ORTBEZ27 -> name
# - KANTON -> canton
dfCity = pd.read_csv(io.StringIO(filteredLines), sep=';', header=None, encoding='cp1252', usecols=[2,3,4,5,8,9], names=['community_nr', 'zip_type', 'zip', 'zip_additional_nr', 'name', 'canton'])
# zip_additional_nr has a leading zero
dfCity['zip_additional_nr'] = dfCity['zip_additional_nr'].apply('{:0>2}'.format)
dfCityCoordinate['zip_additional_nr'] = dfCityCoordinate['zip_additional_nr'].apply('{:0>2}'.format)
dfCity = pd.merge(dfCity, dfCityDistrict, how='left', on=['community_nr'])
dfCity = pd.merge(dfCity, dfCityCoordinate, how='left', on=['zip', 'zip_additional_nr'])
# explicit set Int64 as type because cities in Liechtenstein doesn't have a district nr
dfCity['district_nr'] = dfCity['district_nr'].astype('Int64')
# filter out Campione d'Italia and Büsingen which have suisse zip codes
dfCity = dfCity.query('canton == @CANTONS | canton == "FL"')
dfCity = dfCity.query('zip_type == @ALLOWED_ZIP_TYPES')
dfCity['iso_code'] = ['FL' if x == 'FL' else 'CH' for x in dfCity['canton']]
dfCity['search_term'] = dfCity['name'].apply(lambda x: x.upper())
# if dataset does not contain a coordinate manually set a value
dfCity['latitude'] = [COORDINATES.get(zip, {}).get('latitude') if math.isnan(float(lat)) else lat for lat, zip in zip(dfCity['latitude'], dfCity['zip'])]
dfCity['longitude'] = [COORDINATES.get(zip, {}).get('longitude') if math.isnan(float(lng)) else lng for lng, zip in zip(dfCity['longitude'], dfCity['zip'])]
missingCoordinates = dfCity.query('latitude.isnull() or longitude.isnull()')
if not missingCoordinates.empty:
    raise AssertionError('The cities with code zip '
                         + missingCoordinates.sort_values(by=['zip'])['zip'].to_string(index=False).replace("\n", ", ")
                         + " has no coordinates. Manually add them to the variable COORDINATES.")

dfCity = dfCity.sort_values(by=['zip', 'zip_additional_nr'])
dfCity.to_csv('database/city.csv', index=False, header=False,
              columns=[
                'iso_code',
                'zip',
                'zip_additional_nr',
                'name',
                'search_term',
                'canton',
                'district_nr',
                'community_nr',
                'latitude',
                'longitude'
              ])
